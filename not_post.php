<?php/*
$mysqli = new mysqli("localhost", "my_user", "my_password", "world");

// verificar la conexión 
if (mysqli_connect_errno()) {
    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = "SELECT Name, CountryCode FROM City ORDER by ID LIMIT 3";
$result = $mysqli->query($query);

// array numérico 
$row = $result->fetch_array(MYSQLI_NUM);
printf ("%s (%s)\n", $row[0], $row[1]);

// array asociativo 
$row = $result->fetch_array(MYSQLI_ASSOC);
printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);

// array numérico y asociativo 
$row = $result->fetch_array(MYSQLI_BOTH);
printf ("%s (%s)\n", $row[0], $row["CountryCode"]);

// liberar la serie de resultados 
$result->free();



$resultado = mysql_query("SELECT id, email FROM people WHERE id = '42'");
if (!$resultado) {
    echo 'No se pudo ejecutar la consulta: ' . mysql_error();
    exit;
}
$fila = mysql_fetch_row($resultado);

echo $fila[0]; // 42
echo $fila[1]; // el valor de email

// cerrar la conexión 
$mysqli->close();