-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-03-2019 a las 23:57:27
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_conta`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrar` ()  NO SQL
SELECT p.id_partida,c.nombre_cuenta,cp.monto,tp.nombre_tipo_partida,s.nombre FROM cuenta_partida cp INNER JOIN session s ON cp.id_usuario = s.id_usuario INNER JOIN tipo_partida tp ON cp.id_tipo = tp.id_tipo_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cp.estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `partida` ()  NO SQL
SELECT p.id_partida as partida,p.fecha,p.descripcion,c.nombre_cuenta as cuenta,cp.monto,cp.operacion FROM partida p INNER join cuenta_partida cp on p.id_partida=cp.id_cuenta_partida INNER join cuenta c on cp.id_cuenta=c.id_cuenta$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id_cuenta` int(11) NOT NULL,
  `nombre_cuenta` varchar(45) NOT NULL,
  `id_mayor` int(11) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id_cuenta`, `nombre_cuenta`, `id_mayor`, `orden`, `type`, `saldo`) VALUES
(1, 'Activo', NULL, 1, 2, 0),
(2, 'Pasivo', NULL, 2, 1, 0),
(3, 'Capital', NULL, 1, 1, 0),
(4, 'Ingresos', NULL, 1, 1, 0),
(5, 'Costos', NULL, 2, 2, 0),
(6, 'Gastos', NULL, 2, 2, 0),
(11, 'Activo Corriente', 1, 1, 2, 0),
(12, 'Propiedad, Planta y Equipos', 1, 1, 2, 0),
(13, 'Otros Activos', 1, 1, 2, 0),
(21, 'Pasivos a Corto Plazo', 2, 2, 1, 0),
(22, 'Pasivos a Largo Plazo', 2, 2, 1, 0),
(31, 'Capital', 3, 1, 1, 0),
(32, 'Utilidades Acumuladas', 3, 1, 1, 0),
(33, 'Utilidad Neta', 3, 1, 1, 0),
(34, 'Reserva Legal', 3, 1, 1, 0),
(41, 'Venta de Mercancía', 4, 1, 1, 0),
(42, 'Devoluciones en Ventas', 4, 1, 1, 0),
(43, 'Otros Ingresos', 4, 1, 1, 0),
(51, 'Compras de Mercancías', 5, 2, 2, 0),
(52, 'Devolución en Compras', 5, 2, 2, 0),
(61, 'Gastos de Ventas', 6, 2, 2, 0),
(62, 'Gastos Generales y Administrativos', 6, 2, 2, 0),
(63, 'Gastos Financieros', 6, 2, 2, 0),
(1101, 'Efectivo', 11, 1, 2, 0),
(1102, 'Cuentas por Cobrar', 11, 1, 2, 0),
(1103, 'Inventario', 11, 1, 2, 0),
(1104, 'Gastos Pagados por Adelantado', 11, 1, 2, 0),
(1105, 'Caja', 11, 2, 2, 0),
(1106, 'Banco', 11, 2, 2, 0),
(1201, 'Terrenos', 12, 1, 2, 0),
(1202, 'Edificaciones', 12, 1, 2, 0),
(1203, 'Depreciación Acumulada Edificaciones', 12, 1, 2, 0),
(1208, 'prueba', 11, 1, 2, 0),
(1301, 'Patentes', 13, 1, 2, 0),
(2101, 'Cuentas por Pagar', 21, 2, 1, 0),
(2102, 'ITBIS por Pagar', 21, 2, 1, 0),
(2103, 'Intereses por Pagar', 21, 2, 1, 0),
(2201, 'Documentos por Pagar', 22, 2, 1, 0),
(2202, 'Hipoteca por Pagar', 22, 2, 1, 0),
(3101, 'Patrimonio', 31, 1, 0, 0),
(6101, 'Sueldos', 61, 2, 2, 0),
(6102, 'Publicidad', 61, 2, 2, 0),
(6103, 'Comisiones', 61, 2, 2, 0),
(6201, 'Sueldos', 62, 2, 2, 0),
(6202, 'Electricidad', 62, 2, 2, 0),
(6203, 'Seguro', 62, 2, 2, 0),
(6204, 'Depreciación Edificaciones', 62, 2, 2, 0),
(6205, 'Cuentas Incobrables', 62, 2, 2, 0),
(6301, 'Gastos de Intereses', 63, 2, 0, 0),
(12002, 'prueba', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_detalle`
--

CREATE TABLE `cuenta_detalle` (
  `id_cuenta` int(11) NOT NULL,
  `id_detalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_partida`
--

CREATE TABLE `cuenta_partida` (
  `id_cuenta_partida` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `monto` double NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta_partida`
--

INSERT INTO `cuenta_partida` (`id_cuenta_partida`, `id`, `id_cuenta`, `monto`, `id_tipo`, `id_usuario`, `estado`) VALUES
(1, 28, 1105, 500, 2, 4, 2),
(2, 28, 1106, 10000, 2, 4, 2),
(3, 28, 1102, 1000, 2, 4, 2),
(4, 28, 3101, 11500, 3, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `id_detalle` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `detalle` varchar(45) NOT NULL,
  `cuenta` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `operacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `telefono` int(11) NOT NULL,
  `ubicacion` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id_empresa`, `nombre`, `telefono`, `ubicacion`) VALUES
(0, 'hjm', 48564, 'hjkh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_cuenta`
--

CREATE TABLE `orden_cuenta` (
  `id_orden` int(11) NOT NULL,
  `nombre_orden` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orden_cuenta`
--

INSERT INTO `orden_cuenta` (`id_orden`, `nombre_orden`) VALUES
(1, 'Acreedor'),
(2, 'Deudor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partida`
--

CREATE TABLE `partida` (
  `id_partida` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `partida`
--

INSERT INTO `partida` (`id_partida`, `codigo`, `fecha`, `id_usuario`) VALUES
(1, 2103, '2019-02-08', 2),
(27, 2104, '2019-03-22', 4),
(28, 2105, '2019-03-22', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `session`
--

CREATE TABLE `session` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `usuario` varchar(25) NOT NULL,
  `pass` varchar(60) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `session`
--

INSERT INTO `session` (`id_usuario`, `nombre`, `apellido`, `usuario`, `pass`, `foto`, `id_empresa`) VALUES
(2, 'lea', 'palacios', 'admin', '123', 'libre/assets/img/avatar-female.png', 0),
(3, 'denis', 'ismael', 'cober', '123', 'libre/assets/img/avatar-male.png', 0),
(4, 'Kevin', 'Ramirez', 'chiki123', '123', '/libre/assets/img/uno.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_partida`
--

CREATE TABLE `tipo_partida` (
  `id_tipo_partida` int(11) NOT NULL,
  `nombre_tipo_partida` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_partida`
--

INSERT INTO `tipo_partida` (`id_tipo_partida`, `nombre_tipo_partida`) VALUES
(2, 'Cargo '),
(3, 'Abono ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD KEY `id_mayor` (`id_mayor`),
  ADD KEY `orden` (`orden`);

--
-- Indices de la tabla `cuenta_detalle`
--
ALTER TABLE `cuenta_detalle`
  ADD KEY `id_cuenta` (`id_cuenta`),
  ADD KEY `id_detalle` (`id_detalle`);

--
-- Indices de la tabla `cuenta_partida`
--
ALTER TABLE `cuenta_partida`
  ADD PRIMARY KEY (`id_cuenta_partida`),
  ADD KEY `id_cuenta` (`id_cuenta`),
  ADD KEY `id` (`id`),
  ADD KEY `id_tipo` (`id_tipo`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `orden_cuenta`
--
ALTER TABLE `orden_cuenta`
  ADD PRIMARY KEY (`id_orden`);

--
-- Indices de la tabla `partida`
--
ALTER TABLE `partida`
  ADD PRIMARY KEY (`id_partida`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `tipo_partida`
--
ALTER TABLE `tipo_partida`
  ADD PRIMARY KEY (`id_tipo_partida`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuenta_partida`
--
ALTER TABLE `cuenta_partida`
  MODIFY `id_cuenta_partida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orden_cuenta`
--
ALTER TABLE `orden_cuenta`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `partida`
--
ALTER TABLE `partida`
  MODIFY `id_partida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `session`
--
ALTER TABLE `session`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_partida`
--
ALTER TABLE `tipo_partida`
  MODIFY `id_tipo_partida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`id_mayor`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `cuenta_ibfk_2` FOREIGN KEY (`orden`) REFERENCES `orden_cuenta` (`id_orden`);

--
-- Filtros para la tabla `cuenta_detalle`
--
ALTER TABLE `cuenta_detalle`
  ADD CONSTRAINT `cuenta_detalle_ibfk_1` FOREIGN KEY (`id_cuenta`) REFERENCES `cuenta` (`id_cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cuenta_detalle_ibfk_2` FOREIGN KEY (`id_detalle`) REFERENCES `detalle` (`id_detalle`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuenta_partida`
--
ALTER TABLE `cuenta_partida`
  ADD CONSTRAINT `cuenta_partida_ibfk_2` FOREIGN KEY (`id_cuenta`) REFERENCES `cuenta` (`id_cuenta`),
  ADD CONSTRAINT `cuenta_partida_ibfk_3` FOREIGN KEY (`id`) REFERENCES `partida` (`id_partida`),
  ADD CONSTRAINT `cuenta_partida_ibfk_4` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_partida` (`id_tipo_partida`),
  ADD CONSTRAINT `cuenta_partida_ibfk_5` FOREIGN KEY (`id_usuario`) REFERENCES `session` (`id_usuario`);

--
-- Filtros para la tabla `partida`
--
ALTER TABLE `partida`
  ADD CONSTRAINT `partida_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `session` (`id_usuario`);

--
-- Filtros para la tabla `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id_empresa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
