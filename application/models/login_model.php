<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_model extends CI_Model{

	public function getData()
	{
		$usuarios = $this->db->get('session');
		return $usuarios->result(); 
	}

	public function validar_login($usuario, $pass)
	{
		$this->db->where('usuario',$usuario);
		$this->db->where('pass',$pass);
		$resultado = $this->db->get('session');
		if($resultado->num_rows()>0){
			return $resultado->row();
		}else{
			return false;
		}
	}

	public function get_usuario($user)
	{
		$usuarios = $this->db->query("SELECT * FROM session WHERE usuario = '".$user."'");
		return $usuarios->row(); 
	}
}