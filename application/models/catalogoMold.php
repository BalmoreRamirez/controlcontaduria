<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class catalogoMold extends CI_Model{
	
//===========================================Cuentas Activo ================================================================//
	public function cuenta_activo()
	{
		$cuenta_activo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta=1');
		return $cuenta_activo->result_array();
	}

	public function activo_corriente()
	{
		$activo_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 11 ');
		return $activo_corriente->result_array();
	}

	public function cuentas_activo_corriente()
	{
		$cuentas_activo_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor= 11 ');
		return $cuentas_activo_corriente->result_array();
	}

	public function activo_no_corriente()
	{
		$activo_no_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 12');
		return $activo_no_corriente->result_array();
	}

	public function cuentas_activos_no_corrientes()
	{
		$cuentas_activos_no_corrientes = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor= 12');
		return $cuentas_activos_no_corrientes->result_array();
	}

	public function otras_cuentas()
	{
		$otras_cuentas = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 13 ');
		return $otras_cuentas->result_array();
	}

	public function cuentas_otras_cuentas()
	{
		$cuentas_otras_cuentas = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor= 13');
		return $cuentas_otras_cuentas->result_array();
	}

//=================================================Cuentas Pasivo===================================================================//


	public function cuenta_pasivo()
	{
		$cuenta_pasivo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta=2');
		return $cuenta_pasivo->result_array();
	}

	public function pasivo_a_corto_plazo()
	{
		$pasivo_a_corto_plazo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta=21');
		return $pasivo_a_corto_plazo->result_array();
	}

	public function cuentas_pasivo_a_corto_plazo()
	{
		$cuentas_pasivo_a_corto_plazo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta  WHERE id_mayor= 12');
		return $cuentas_pasivo_a_corto_plazo->result_array();
	}

	public function pasivo_a_largo_plazo()
	{
		$pasivo_a_largo_plazo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta  WHERE id_cuenta= 22');
		return $pasivo_a_largo_plazo->result_array();
	}

	public function cuentas_pasivo_a_largo_plazo()
	{
		$cuentas_pasivo_a_largo_plazo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta  WHERE id_mayor=22');
		return $cuentas_pasivo_a_largo_plazo->result_array();
	}

//============================================ Capital ====================================================================//


	public function capital()
	{
		$capital = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 3');
		return $capital->result_array();
	}

	public function cuentas_capital()
	{
		$cuentas_capital = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor = 3');
		return $cuentas_capital->result_array();
	}

//======================================================Ingresos===============================================================//

	public function ingresos()
	{
		$ingresos = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 4');
		return $ingresos->result_array();
	}

	public function ingresos_cuentas()
	{
		$ingresos_cuentas = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor = 4');
		return $ingresos_cuentas->result_array();
	}
//====================================================== costos ===========================================================//

public function costo()
	{
		$costo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 5');
		return $costo->result_array();
	}

//==================================================== gastos =========================================================0//

	public function gastos()
	{
		$gastos = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 6');
		return $gastos->result_array();
	}

	public function gastos_cuentas()
	{
		$gastos_cuentas = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_mayor = 6');
		return $gastos_cuentas->result_array();
	}

//=============================== PROCESO DE GUARDAR CATALOGO =============================================

	public function cuenta_perteneciente()
	{
		$mayor = $this->db->query('SELECT id_cuenta, nombre_cuenta FROM cuenta WHERE id_cuenta BETWEEN 11 AND 63');
		return $mayor->result_array();
	}

	public function tipo_de_cuenta()
	{
		$orden = $this->db->query('SELECT id_orden,nombre_orden FROM orden_cuenta');
		return $orden->result_array();
	}

	public function insertar($data)
	{
		return $this->db->insert('cuenta',$data);
	}

}