<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class partidas_model extends CI_Model
{
	public function codigo(){
		$query = $this->db->query('SELECT * FROM partida WHERE id_partida = (SELECT MAX(id_partida) FROM partida)');
		return $query->row();
	}

	public function max_id(){
		$maximo = $this->db->query('SELECT MAX(id_partida) as max FROM partida');
		return $maximo->row();
	}
	public function sel_mayo($id_mayor){
		$maximo = $this->db->query('SELECT * FROM `cuenta` WHERE id_mayor ='.$id_mayor);
		return $maximo->result_array();
	}
	public function sel_sub($id_mayor){
		$maximo = $this->db->query('SELECT * FROM `cuenta` WHERE id_mayor ='.$id_mayor);
		return $maximo->result_array();
	}
	public function buscar($id_usuario){
		$maximo = $this->db->query("SELECT * FROM cuenta_partida cp  WHERE estado = 1 AND id_usuario ='".$id_usuario."'");
		if ($maximo->row() > 0) {
			return $maximo->row();
		}else{
			return "Error";
		}
		
	}
	public function tipo()
	{
		//$this->db->where('id_cuenta BETWEEN 1 AND 6');
		$tipopar = $this->db->get('tipo_partida');		
		return $tipopar->result_array();
	}

	public function guardar_partidas($fecha,$descripcion,$tipo_partida)
	{
		$data = array(

			'fecha' => $fecha,
			'descripcion' => $descripcion,
			'tipo_partida' => $tipo_partida,
		);
		$this->db->insert('partida',$data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	public function cuenta()
	{
		$this->db->where('id_cuenta BETWEEN 1 AND 6');
		$cuenta = $this->db->get('cuenta');
		return $cuenta->result_array();
	}

	public function guardar_cuentas_partidas($id_cuenta,$monto,$operacion)
	{
		$data = array(

			'id_cuenta' => $id_cuenta,
			'monto' => $monto,
			'operacion' => $operacion
		);
		$this->db->insert('cuenta_partida',$data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}
	public function getpartidas(){
		$partida=$this->db->query('SELECT p.id_partida as partida,p.fecha,p.descripcion,c.nombre_cuenta as cuenta,cp.monto FROM partida p INNER join cuenta_partida cp on p.id_partida=cp.id_cuenta_partida INNER join cuenta c on cp.id_cuenta=c.id_cuenta');
		return $partida->result_array();
	}
	public function guardar_partida($data){

		$this->db->insert('partida',$data);
	}
	public function partida_guardar($data,$primaria){
		
		$this->db->set('id',$data['id']);
		$this->db->set('id_usuario',$data['id_usuario'] );
		$this->db->set('id_tipo',$data['id_tipo']);
		$this->db->set('estado',$data['estado']);
		$this->db->set('id_cuenta',$primaria);
		$this->db->set('fecha_emitida',$data['fecha']);
		$this->db->set('fecha_ingresada',$data['fechas']);
		$this->db->set('monto',$data['monto']);
		$this->db->insert('cuenta_partida');
	}
	public function partida_guardas($data,$secundaria){
		$this->db->set('id',$data['id']);
		$this->db->set('id_usuario',$data['id_usuario'] );
		$this->db->set('id_tipo',$data['id_tipo']);
		$this->db->set('estado',$data['estado']);
		$this->db->set('id_cuenta',$secundaria);
		$this->db->set('fecha_emitida',$data['fecha']);
		$this->db->set('fecha_ingresada',$data['fechas']);
		$this->db->set('monto',$data['monto']);
		$this->db->insert('cuenta_partida');
	}

	public function mostrarse($id_user,$id_max)
	{
		$partidas = $this->db->query("SELECT p.id_partida,c.nombre_cuenta,cp.monto,tp.nombre_tipo_partida,s.nombre FROM cuenta_partida cp INNER JOIN session s ON cp.id_usuario = s.id_usuario INNER JOIN tipo_partida tp ON cp.id_tipo = tp.id_tipo_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cp.estado = 1 AND s.id_usuario ='".$id_user."'AND cp.id ='".$id_max."'");
		return $partidas->result_array();
	}
	public function extraer_par_cuenta($id_user,$id_max)
	{
		$partidas = $this->db->query("SELECT * FROM cuenta_partida WHERE estado = 1 AND id_usuario = '".$id_user."' AND id = '".$id_max."'");
		return $partidas->result_array();
	}
	public function max_partida($id_user)
	{
		$partidas = $this->db->query("SELECT MAX(id_partida) as max FROM partida WHERE id_usuario  ='".$id_user."'");
		return $partidas->row();
	}

	public function insertar()
	{
		$this->db->insert('asciento_contable',$data);
	}
	public function insert_partida($data)
	{
		$this->db->insert('partida',$data);
	}
	public function update_cuenta_par($data,$id)
	{	$this->db->set('estado',$data);
		$this->db->where('id_cuenta_partida',$id);
		$this->db->update('cuenta_partida');
	}
}


