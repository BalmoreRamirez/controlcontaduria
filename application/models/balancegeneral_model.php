<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class balancegeneral_model extends CI_Model
{
	public function empresa()
	{
		$empresa = $this->db->query('SELECT nombre_e FROM empresa');
		return $empresa->row();
	}

	public function activo()
	{
		$partidas = $this->db->query('SELECT * FROM cuenta_partida cp INNER JOIN cuenta cuen ON cp.id_cuenta = cuen.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cuen.id_mayor= 11');
		return $partidas->result_array();
	}

	public function total_a()
	{
		$total = $this->db->query('SELECT *,sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor BETWEEN 11 AND 13');
		return $total->result_array();
	}

	public function activo_corriente()
	{
		$activo_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 11 ');
		return $activo_corriente->result_array();
	}

	public function activo_no_corriente()
	{
		$activo_no_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 12');
		return $activo_no_corriente->result_array();
	}

	public function activo_fijo()
	{
		$activo_fijo = $this->db->query('SELECT * FROM cuenta_partida cp INNER JOIN cuenta cuen ON cp.id_cuenta = cuen.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cuen.id_mayor BETWEEN 12 AND 13');
		return $activo_fijo->result_array();
	}

	public function total_activo_fijo()
	{
		$total = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor BETWEEN 12 AND 13');
		return $total->result_array();
	}

	public function total_activo()
	{
		$partidas = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor BETWEEN 11 AND 13');
		return $partidas->result_array();
	}

//==================================== PASIVO ==============================================================

	public function pasivo_corriente()
	{
		$pasivo_corriente = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 21 ');
		return $pasivo_corriente->result_array();
	}

	public function pasivo()
	{
		$cuenta = $this->db->query('SELECT * FROM cuenta_partida cp INNER JOIN cuenta cuen ON cp.id_cuenta = cuen.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cuen.id_mayor in(62,63)');
		return $cuenta->result_array();
	}

	public function total_p()
	{
		$total = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor IN(12,21,22,61,62,63,1)');
		return $total->result_array();
	}

	public function pasivo_largo_plazo()
	{
		$pasivo_largo_plazo = $this->db->query('SELECT id_cuenta,nombre_cuenta FROM cuenta WHERE id_cuenta = 22 ');
		return $pasivo_largo_plazo->result_array();
	}

	public function mas_pasivo_largo_plazo()
	{
		$cuenta = $this->db->query('SELECT * FROM cuenta_partida cp INNER JOIN cuenta cuen ON cp.id_cuenta = cuen.id_cuenta INNER JOIN partida p ON cp.id = p.id_partida WHERE cuen.id_mayor =22');
		return $cuenta->result_array();
	}

	public function total_p_largo_plazo()
	{
		$total = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor = 22');
		return $total->result_array();
	}

//===================================== CAPITAL =====================================================

	public function patrimonio_cuentas()
	{
		$patrimonio_cuentas = $this->db->query('SELECT * FROM cuenta_partida cp INNER join partida p on cp.id=p.id_partida INNER JOIN cuenta c on cp.id_cuenta=c.id_cuenta where c.id_mayor=31');
		return $patrimonio_cuentas->result_array();
	}

	public function total_patrimonio()
	{
		$total = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor = 31');
		return $total->result_array();
	}

//===================================  CAPITAL + PASIVO ================================================

	public function total_patrimonio_mas_pasivo()
	{
		$total = $this->db->query('SELECT sum(cp.monto) as Total FROM cuenta_partida cp INNER join cuenta c on cp.id_cuenta=c.id_cuenta WHERE c.id_mayor BETWEEN 21 AND 63');
		return $total->result_array();
	}

//============================================= PDF ======================================================

}