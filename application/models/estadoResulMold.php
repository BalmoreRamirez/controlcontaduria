<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class estadoResulMold extends CI_Model{

	public function cargarInfo($de,$acta){
		$query = $this->db->query("
			SELECT cp.fecha_emitida,c.nombre_cuenta,cp.monto 
			FROM partida p 
			INNER JOIN cuenta_partida cp on p.id_partida = cp.id
			INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta
			WHERE cp.fecha_emitida BETWEEN '".$de."' AND '".$acta."'");
		return $query->row();
	}

	public function ingresos($de,$acta){
		$query = $this->db->query("SELECT SUM(cp.monto) as m FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 31 and cp.fecha_emitida BETWEEN '".$de."' AND '".$acta."'");
		return $query->row();
	}

	public function costos($de,$acta){
		$query = $this->db->query("SELECT SUM(cp.monto) as co FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 5 and cp.fecha_emitida BETWEEN '".$de."' AND '".$acta."'");
		return $query->row();
	}

	public function gastos($de,$acta){
		$query = $this->db->query("SELECT SUM(cp.monto) as gas FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor BETWEEN 61 AND 63 and cp.fecha_emitida BETWEEN '".$de."' AND '".$acta."'");
		return $query->row();
	}

	public function intereses($de,$acta){
		$query = $this->db->query("SELECT SUM(cp.monto) as inte FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 31 and cp.fecha_emitida BETWEEN '".$de."' AND '".$acta."'");
		return $query->row();
	}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	public function ingresosp(){
		$query = $this->db->query("SELECT SUM(cp.monto) as m FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 31 and cp.fecha_emitida");
		return $query->row();
	}

	public function costosp(){
		$query = $this->db->query("SELECT SUM(cp.monto) as co FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 5 and cp.fecha_emitida");
		return $query->row();
	}

	public function gastosp(){
		$query = $this->db->query("SELECT SUM(cp.monto) as gas FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor BETWEEN 61 AND 63 and cp.fecha_emitida");
		return $query->row();
	}

	public function interesesp(){
		$query = $this->db->query("SELECT SUM(cp.monto) as inte FROM cuenta_partida cp INNER JOIN partida p ON cp.id = p.id_partida INNER JOIN cuenta c ON cp.id_cuenta = c.id_cuenta WHERE c.id_mayor = 31 and cp.fecha_emitida");
		return $query->row();
	}






}