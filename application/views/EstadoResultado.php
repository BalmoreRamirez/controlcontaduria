<style>
	.container{
		text-align:center
	}
	.left{
		float: left;

	}
	.right{
		float: right;

	}
	.center{

		display:inline-block;
	}
</style>
<section class="full-width">
	<div class="container">
		<div class="row justify-content-md-center my-3">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-header text-white bg-info text-center">
						Generar estado de resultados.
					</div>
					<div class="card-body">
						<div class="row justify-content-md-center">
							<!----------------------------------------------------->
							<div class="col-lg-12 bg-light">
								<form id="estado" >
									<div class="row">

										<div class="col-4">
											<label>DE:</label>
											<input type="date" class="form-control" name="de" id="de">
										</div>

										<div class="col-3">
											<label>HASTA:</label>
											<input type="date" class="form-control" name="acta" id="acta" >
										</div>

										<div class="col-3">
											<label>Procesar</label>
											<button type="button" onclick="estaGe()" class="btn btn-info">
												<i class="fas fa-spinner"></i>
												Procesar
											</button>
										</div>

										<div class="col-2">
											<label>Exportar</label>
											<button class="btn btn-info" type="button"onclick="pdf()"><i class="fa fa-file-pdf fa-1x" ></i></button>
										</div>

									</div>
								</form>
							</div>
							<!------------------------------------------------------>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class='container'>
		<div class='row'>
			<div class='col-lg-12'id="estados">

			</div>
		</div>
	</div>

	<br><br>
	<div class="container">
		<div class="left">
			Firma de contador
		</div>
		<div class="right">
			Firma del auditor

		</div>
		<div class="center">
			Firma del gerente
		</div>
	</div>
</section>
<script type="text/javascript">

/*	
$(document).ready(function(){

	$("input[name=de]").click(function(){
		alert('Evento click sobre un input text con nombre="nombre1"');
	});
    //$("#nombre2").click(function(){
	$("input[id=acta]").click(function(){
		alert('Evento click sobre un input text con id="nombre2"');
	});
});
*/

//====================================
function estaGe(){
	var url='<?php echo base_url();?>estadoResultadoContr/consultar';
	$.ajax({
		url:url,
		type:'post',
		data: $('#estado').serialize(),
		success: function(data){
			$('#estados').html(data);
		}
	});
}
function pdf(){
	window.location="<?php echo base_url(); ?>estadoResultadoContr/Reporte_estado?"+$('#estado').serialize();
}
</script> 