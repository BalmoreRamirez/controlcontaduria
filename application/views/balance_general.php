<style>
.container{
    text-align:center
}
.left{
    float: left;
 
}
.right{
    float: right;

}
.center{

   display:inline-block
}

</style>
<section class="full-width ">

	<div class="full-width ">
		
<div class="card">
	<div class="card-body">
<div class="row">
	<div class="col-lg-12">
				<div class="row justify-content-md-center my-3">
	<div class="col-lg-2">
		<label>Exportar</label><br>
		<a href="<?php echo base_url(); ?>balancegeneral/reporte" class="btn btn-info">
			<i class="far fa-file-pdf fa-2x"></i>
		
	</a>
	</div>
	<hr>
	<div class="col-lg-6">
		<p class="text-condensedLight h2">
			El Balance general 
		</p>
	</div>
</div>
	</div>
</div>
	</div>
</div>
		
	</div>
</section>
<section class="full-width ">

		<div class="container">
			<center>	<h4>Balance general de la empresa <?=$empresa->nombre_e?></h4>
				
	<div class="row">

		<div class="col-lg-6">
			<table class="table">
			<thead>
			<tr>
			<th>Activos</th>
			<th>Cantidad</th>
			</tr>
			</thead>
			<tbody id="activo_corriente" style="background-color: #8cf98f "></tbody>
			<tbody id="ya"></tbody>
			<tbody id="total_activo_corriente"></tbody>
			<tbody id="activo_no_corriente" style="background-color: #8cf98f "></tbody>
			<tbody id="activo_fijo" ></tbody>
			<tbody id="total_activo" ></tbody>
			
			</table>
		</div>

		<div class="col-lg-6">
			<table class="table">
			<thead>
			<tr>
			<th>Pasivo</th>
			<th>Cantidad</th>
			</tr>
			</thead>
			<tbody id="pasivo_corriente" style="background-color: #8cf98f "></tbody>
			<tbody id="general"></tbody>
			<tbody id="total2"></tbody>
			<tbody id="pasivo_largo_plazo" style="background-color: #8cf98f "></tbody>
			<tbody id="mas_pasivo_largo_plazo"></tbody>
			<tbody id="tota_largo_plazo"></tbody>
			<tbody id="patrimonio"></tbody>
			<tbody></tbody>
			<tbody></tbody>
			<tbody id="patrimonio_cuentas"></tbody>
			<tbody id="total_patrimonio"></tbody>
			<tbody id="total_patrimonio_mas_capital"></tbody>
			</tr>

			</table>
		</div>
	</div>
</center>
<br><br><br>
<div class="container">
<div class="left">
Firma de contador
    </div>
<div class="right">
Firma del auditor
         
    </div>
<div class="center">
Firma del gerente
    </div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		activo_corriente();
		activo_no_corriente();
		activo_fijo();
		total_activo_fijo();
		total_activo();
		activos();
		total_activos();

//==================================================================================================
		pasivo_corriente();
		pasivos();
		total_pasivos();
		pasivo_largo_plazo();
		mas_pasivo_largo_plazo();
		total_pasivos_largo_plazo();


//==================================================================================================
		
		patrimonio();
		patrimonio_cuentas();
		total_patrimonio();
		total_patrimonio_mas_capital();
	});

	function activos(){
		var url='<?php echo base_url(); ?>balancegeneral/activos';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#ya').html(data);
			}
		});
	}

	

	function total_activos(){
		var url='<?php echo base_url(); ?>balancegeneral/total_activos';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#total_activo_corriente').html(data);
			}
		});
	}

	function activo_corriente(){
	var url = '<?php echo base_url(); ?>balancegeneral/activo_corriente';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#activo_corriente').html(data);
		}
	});
}

function activo_no_corriente(){
	var url= '<?php echo base_url(); ?>balancegeneral/activo_no_corriente';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#activo_no_corriente').html(data);
		}
	});
}

function activo_fijo(){
	var url= '<?php echo base_url(); ?>balancegeneral/activo_fijo';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#activo_fijo').html(data);
		}
	});
}

function total_activo_fijo(){
	var url= '<?php echo base_url(); ?>balancegeneral/total_activo_fijo';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#activo_fijo').html(data);
		}
	});
}

function total_activo(){
	var url= '<?php echo base_url(); ?>balancegeneral/total_activo';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#total_activo').html(data);
		}
	});
}

//=================================== PASIVO ==========================================

function pasivos(){
		var url='<?php echo base_url(); ?>balancegeneral/pasivos';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#general').html(data);
			}
		});
	}

function total_pasivos(){
		var url='<?php echo base_url(); ?>balancegeneral/total_pasivos';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#total2').html(data);
			}
		});
	}

function pasivo_corriente(){
	var url = '<?php echo base_url(); ?>balancegeneral/pasivo_corriente';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#pasivo_corriente').html(data);
		}
	});
}

function pasivo_largo_plazo(){
	var url = '<?php echo base_url(); ?>balancegeneral/pasivo_largo_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#pasivo_largo_plazo').html(data);
		}
	});
}

function mas_pasivo_largo_plazo(){
	var url = '<?php echo base_url(); ?>balancegeneral/mas_pasivo_largo_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#mas_pasivo_largo_plazo').html(data);
		}
	});
}

function total_pasivos_largo_plazo(){
		var url='<?php echo base_url(); ?>balancegeneral/total_pasivos_largo_plazo';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#tota_largo_plazo').html(data);
			}
		});
	}

//================================== CAPITAL ======================================================

function patrimonio(){
		var url='<?php echo base_url(); ?>balancegeneral/patrimonio';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#patrimonio').html(data);
			}
		});
	}

	function patrimonio_cuentas(){
		var url='<?php echo base_url(); ?>balancegeneral/patrimonio_cuentas';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#patrimonio_cuentas').html(data);
			}
		});
	}

function total_patrimonio(){
		var url='<?php echo base_url(); ?>balancegeneral/total_patrimonio';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#total_patrimonio').html(data);
			}
		});
	}

	function total_patrimonio_mas_capital(){
		var url='<?php echo base_url(); ?>balancegeneral/total_patrimonio_mas_capital';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#total_patrimonio_mas_capital').html(data);
			}
		});
	}

</script>
