<!--======================================================================================================-->
<section class="full-width navLateral">

		<div class="full-width navLateral-bg btn-menu"></div>
		<div class="full-width navLateral-body">
			<div class="full-width navLateral-body-logo text-center tittles bg-info">
				<i class="zmdi zmdi-close btn-menu"></i> Accounting Team 
			</div>
			<figure class="full-width navLateral-body-tittle-menu">
				<div>
					<img src="<?=base_url();?><?=$user->foto?>" alt="Avatar" class="img-responsive rounded-circle">
				</div>
				<figcaption>
					<span>
						<?=$user->nombre?> <?=$user->apellido?><br>						
					</span>

				</figcaption>
			</figure>
			<nav class="full-width">
				<ul class="full-width list-unstyle menu-principal">
					<li class="full-width">
						<a href="<?=base_url();?>Inicial" class="full-width">
							<div class="navLateral-body-cl">
								<i class="zmdi zmdi-view-dashboard"></i>
							</div>
							<div class="navLateral-body-cr">
								HOME
							</div>
						</a>
					</li>
					<li class="full-width divider-menu-h"></li>					
					<li class="full-width">
						<a href="#" class="full-width btn-subMenu">
							<div class="navLateral-body-cl">
								<i class="zmdi zmdi-case"></i>
							</div>
							<div class="navLateral-body-cr">
								CATALOGOS
							</div>
							<span class="zmdi zmdi-chevron-left"></span>
						</a>
						<ul class="full-width menu-principal sub-menu-options">
							<li class="full-width">
								<a href="<?=base_url();?>Inicial/CantaCuentas" class="full-width">
									<div class="navLateral-body-cl">
										<i class="fas fa-arrow-circle-right"></i>
									</div>
									<div class="navLateral-body-cr">
										Catalagos de cuenta
									</div>
								</a>
							</li>						
						</ul>
					</li>
					<li class="full-width divider-menu-h"></li>
						<li class="full-width">
						<a href="#" class="full-width btn-subMenu">
							<div class="navLateral-body-cl">
							<i class="fas fa-funnel-dollar"></i>
							</div>
							<div class="navLateral-body-cr">
								BALANCE
							</div>
							<span class="zmdi zmdi-chevron-left"></span>
						</a>
						<ul class="full-width menu-principal sub-menu-options">
							<li class="full-width">
								<a href="<?=base_url();?>Inicial/BalanceCompro" class="full-width">
									<div class="navLateral-body-cl">
									
									<i class="fas fa-arrow-circle-right"></i>
									</div>
									<div class="navLateral-body-cr">

										Balance comprobacion
									</div>
								</a>
							</li>
							<li class="full-width divider-menu-h"></li>
							<li class="full-width">
								<a href="<?=base_url();?>Inicial/balance_general" class="full-width">
									<div class="navLateral-body-cl">
									<i class="fas fa-arrow-circle-right"></i>
									</div>
									<div class="navLateral-body-cr">
										Balance General
									</div>
								</a>
							</li>										
						</ul>
					</li>


					<li class="full-width divider-menu-h"></li>
						<li class="full-width">
						<a href="#" class="full-width btn-subMenu">
							<div class="navLateral-body-cl">
							<i class = "zmdi zmdi-mall"> </i>
							</div>
							<div class="navLateral-body-cr">
								PARTIDAS
							</div>
							<span class="zmdi zmdi-chevron-left"></span>
						</a>
						<ul class="full-width menu-principal sub-menu-options">
							<li class="full-width">
								<a href="<?=base_url();?>Inicial/Partidas" class="full-width">
									<div class="navLateral-body-cl">
										<i class="fas fa-arrow-circle-right"></i>
									</div>
									<div class="navLateral-body-cr">
										Partidas
									</div>
								</a>
							</li>							
						</ul>
					</li>

					<!------->
					<li class="full-width divider-menu-h"></li>
						<li class="full-width">
						<a href="#" class="full-width btn-subMenu">
							<div class="navLateral-body-cl">
							<i class="fas fa-file-invoice"></i>
							</div>
							<div class="navLateral-body-cr">
								ESTADOS
							</div>
							<span class="zmdi zmdi-chevron-left"></span>
						</a>
						<ul class="full-width menu-principal sub-menu-options">
							<li class="full-width">
								<a href="<?=base_url();?>Inicial/EstadoResultaso" class="full-width">
									<div class="navLateral-body-cl">
										<i class="fas fa-arrow-circle-right"></i>
									</div>
									<div class="navLateral-body-cr">
										RESULTADO
									</div>
								</a>
							</li>								
						</ul>
					</li>


			
				
			</nav>
		</div>
	</section>
						
	<section class="full-width pageContent">
		<!-- navBar -->
		<div class="full-width navBar">
			<div class="full-width navBar-options">
				<i class="zmdi zmdi-swap btn-menu" id="btn-menu"></i>	
				<div class="mdl-tooltip" for="btn-menu">Ocular menu</div>
				<nav class="navBar-options-list">
					<ul class="list-unstyle">
						
						<li class="btn-exit" id="btn-exit">
							<i class="zmdi zmdi-power"></i>
							<div class="mdl-tooltip" for="btn-exit">Salir</div>
						</li>
						<li class="text-condensedLight noLink" ><small><?=$user->nombre?> <?=$user->apellido?></small></li>
						<li class="noLink">
							<figure>
								<img src="<?=base_url();?><?=$user->foto?>" alt="Avatar" class="img-responsive">
							</figure>
						</li>
					</ul>
				</nav>
			</div>
		</div>

	<!--======================================================================================================-->