	<!-- Modal -->
	<div class="modal fade datosxd" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				
				<div class="modal-body">

					<div class="full-width panel-content">
						<form id="guardar">
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<legend class="text-condensedLight"><i class="zmdi zmdi-border-color"></i> &nbsp; Formulario</legend><br>
								</div>
								<div class="mdl-cell mdl-cell--12-col">

									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" class='input-group '  name="id_partida" type="number"  id="id_partida">
										<label class="mdl-textfield__label" for="id_partida">N° de partida</label>
										
									</div>

									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" class='input-group date'  name="fecha" type="date"  id="fecha">
										<label class="mdl-textfield__label" for="fecha">Fecha</label>
										<span class="mdl-textfield__error">fecha invalida</span>
									</div>
								
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" name="descripcion" id="descripcion">
										<label class="mdl-textfield__label" for="descripcion">Descripcion</label>
										<span class="mdl-textfield__error">Descripcion invalida</span>
									</div>
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select id="select" name="tipo_partida" class="mdl-textfield__input">
											
										</select>
										<label class="mdl-textfield__label" for="LastNameClient">Tipo de partida</label>
										
									</div>
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select id="select_cuenta" name="id_cuenta" class="mdl-textfield__input"  onchange="primary();">
											
										</select>
										<label class="mdl-textfield__label" for="descripcion">cuenta</label>
										
									</div>
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select id="selec_ tipo" name="id_orden" class="mdl-textfield__input" onchange="secundary();">
											
										</select>
										<label class="mdl-textfield__label" for="descripcion">tipo de Cuenta</label>
										
									</div>
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select id="selec_sub" name="id_mayor" class="mdl-textfield__input">
											
										</select>
										<label class="mdl-textfield__label" for="addressClient2">Sub Cuenta</label>
										
									</div>
								</div>
								<div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" name="monto"  id="phoneClient">
										<label class="mdl-textfield__label" for="phoneClient">Monto</label>
										
									</div>
								</div>

								

									<div id="ver">
										
									</div>
								
							</div>
							<p class="text-center">
									<input type="button" onclick="guardar1()" value="Guardar">
								
							</p>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>

<!--======================================================================================================-->



<script type="text/javascript">
		$(document).ready(function(){
			cuenta_partida();
			selectcuenta();
	});

		//------------------------------------------------------------------------------------------//
function cuenta_partida() {
	var  url ='<?php echo base_url();?>partidasCtr/select_cuenta';
	$.ajax({
		type:'post',
		url:url,
		success: function(data){
			$('#select').html(data);
		}
	});
}

//------------------------------------------------------------------------------------------//
function selectcuenta() {
	var  url ='<?php echo base_url();?>partidasCtr/select';
	$.ajax({
		type:'post',
		url:url,
		success: function(data){
			$('#select_cuenta').html(data);
		}
	});
}

//-------------------------------------------------------------------------------------//
function primary(){
	var primary= '<?php echo base_url();?>partidasCtr/select_tipo';
	var id=$('#select_cuenta').val();
	$.ajax({
		type:'get',
		url: primary,
		data:'id='+id,
		success:function(data){

			$('#selec_tipo').html(data);
			secundary();

		}
	});
}

 //------------------------------------------------------------------------------//

  function secundary(){
  	var secundary= '<?php echo base_url();?>partidasCtr/select_sub';
  	var id = $('#selec_tipo').val();
  	$.ajax({
  		type:'get',
  		url: secundary,
  		data:'id='+id,
  		success:function(data){
  			$('#selec_sub').html(data);
  		}
  	});
  }

//-------------------------------------------------------------------------------//

function guardar1(){
			var url ="<?php echo base_url();?>partidasCtr/agregar_partidas";
			$.ajax({
				url:url,
				type:'post',
				data: $('#guardar').serialize(),
				success:function(data){
					$('#exampleModal1').modal('hide')
					$('#detalle').modal('show');
				}
			});
}



</script>

