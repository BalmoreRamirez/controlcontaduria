<section>
	<br>
	<div class="container text-white">
		<a data-toggle="modal" data-target="#exampleModal1" class=" btn btn-info">
			<i class="fas fa-plus-circle">				
			</i> Agregar
		</a> 
		<button onclick="terminar_partida()" class="btn btn-info">
			<i class="fas fa-hourglass-end"></i> 
		 Concluir Partida
		</button>
	</div>
	
	<div class="mdl-tabs__panel is-active" id="tabNewProvider">
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--12-col">
				<div class="full-width panel mdl-shadow--2dp">
					<div class="full-width panel-tittle bg-info text-center tittles">
						<h3>Mostrar datos</h3>
					</div>

					<table class="table">
						<thead>
							<tr>
								<td>Partida</td>
								<td>Cuenta</td>
								<td>Monto</td>
								<td>Tipo de partida</td>
							</tr>
						</thead>
						<tbody id="ya">

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">

	$(document).ready(function(){
		mostrar();
		
	});

	function mostrar(){
		var url = '<?php echo base_url(); ?>partidasCtr/partidas';
		$.ajax({
			type:'post',
			url:url,
			success:function(data){
				$('#ya').html(data);
			}
		});
	}
	function terminar_partida(){
		var url = '<?php echo base_url(); ?>partidasCtr/terminar';
		swal({
			title: 'Quieres concluir con la partida?',
			text: "al concluir con la partida ya no podra agregsar ningiun dato en la partida actual!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3dc4d2',
			cancelButtonColor: '#e71a1a',
			confirmButtonText: 'Desea confirmar!',
			cancelButtonText: 'Cancelar'
		},
		function(isConfirm) {
			if (isConfirm) {
				$.ajax({
					type:'post',
					url:url,
					success:function(data){
						alert('se termino esto!');
						if(opcion == true){
						window.location.href='<?php echo base_url();?>Inicial/Partidas';
						}
					}
				});
			}
		});
	}
</script> 