<style type="text/css">

</style>
<section>
	<div class="mdl-tabs__panel is-active" id="tabNewProvider">
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--1-col"></div>
			<div class="mdl-cell mdl-cell--10-col">
				<br><br><br><br><br><br><br>
				<center>
					<div class="full-width panel mdl-shadow--2dp">
						<div class="full-width panel-tittle bg-info text-center tittles">
							<i class="fas fa-cogs fa-1x"></i>
							GENERAR PARTIDA
						</div>
						<div class="full-width panel-content">
							<br><br>
							<table class="mdl-cell mdl-cell--12-col">
								<thead>
									<tr>
										<th><h3 style="text-align: center;"> PARTIDA : #<?=$dato = $codigo->codigo+1;?></h3></th>
									</tr>
								</thead>
							</table><br><br>
							<div class="mdl-cell mdl-cell--3-col"></div>
							<div class="mdl-cell mdl-cell--6-col">
								<button class="btn btn-info btn-block" onclick="Generar()">
									<i class="fas fa-chevron-circle-right fa-1x"></i>
									Generar Partidas
								</button>
							</div>
							<div class="mdl-cell mdl-cell--3-col"></div>
						</div>
					</div>
				</center>
			</div>
			<div class="mdl-cell mdl-cell--1-col"></div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function Generar(){
		var url = '<?= base_url();?>partidasCtr/buscar';
		$.ajax({
			url : url,
			type : "post",
			success:function(data){
				if (data == 'Error'){
					swal({
						title: 'Quieres Generar Una Partidad?',
						text: "AL genera un Partida se tendra que concluir la anterio caso contrario no podra generar otra Partida el usuario!",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3dc4d2',
						cancelButtonColor: '#e71a1a',
						confirmButtonText: 'Generar Partida!',
						cancelButtonText: 'Cancelar'
					},
					function(isConfirm) {
						if (isConfirm) {
							window.location='<?= base_url();?>partidasCtr/generar_partida'; 
						}
					});
				}else{
					swal({
						type: 'error',
						title: 'Oops...',
						text: 'Se Encontro un dato sin guardar!',
					},
					function(isConfirm) {
						if (isConfirm) {
							window.location='<?= base_url();?>Inicial/tab_cuentas'; 
						}
					});
				}
			}
		});
		
	}
</script>
