<div class="login-wrap cover">
		<div class="container-login">
			<p class="text-center" style="font-size: 80px;">
				<i class="zmdi zmdi-account-circle"></i>
			</p>
			<p class="text-center text-condensedLight">Sistema de Contabilidad General</p>
			<form method="post" action="<?php echo base_url(); ?>loginCtr/validar">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <input class="mdl-textfield__input" type="text" id="usuario" name="usuario">
				    <label class="mdl-textfield__label" for="userName" name="usuario">Usuario</label>
				</div>
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				    <input class="mdl-textfield__input" type="password" id="pass" name="pass">
				    <label class="mdl-textfield__label" for="pass" name="pass">Contraseña</label>
				</div>
				<button class="mdl-button mdl-js-button mdl-js-ripple-effect" style="color: #3F51B5; margin: 0 auto; display: block;">
					Entrar
				</button>
			</form>
		</div>
	</div>