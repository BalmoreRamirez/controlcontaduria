<style type="text/css">
#head{
	background-color:#5cd6d6;
}
</style>
<!-- pageContent -->
<section class="full-width header-well">
	<div class="full-width header-well-icon">
		<i class = "zmdi zmdi-file-text"> </i>
	</div>
	<div class="full-width header-well-text">
		<p class="text-condensedLight">
			La principal característica en el catálogo de cuentas de una empresa comercial está en la parte de los costos, pues a diferencia de una industrial no se incluyen inventario de materiales o productos en proceso.
		</p>
	</div>
</section>
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
	<div class="mdl-tabs__tab-bar">
		<a href="#tabNewProvider" class="mdl-tabs__tab is-active">Crear</a>
		<a href="#tabListProvider" class="mdl-tabs__tab">Listar</a>
	</div>
	<div class="mdl-tabs__panel is-active" id="tabNewProvider">
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--12-col">
				<div class="full-width panel mdl-shadow--2dp">
					<div class="full-width panel-tittle text-center tittles" id="head">

						Catalogo de cuentas
					</div>
					<div class="full-width panel-content">
						<!--Aqui va el formulario-->

						<form id="form">
						
						N° de cuenta:
						<input type="number" name="id_cuenta" id="id_cuenta"   class="form-control">
						<br>
						Nombre de la cuenta:
						<input type="text" name="nombre_cuenta" id="nombre_cuenta"  class="form-control">
						<br>
						Cuenta perteneciente:
						<select name="id_mayor" id="id_mayor"  class="form-control">
							
						</select>
						<br>
						Tipo de cuenta:
						<select name="orden" id="orden"  class="form-control">
							
						</select>
						<br>
						<center><button id="enviar" name="enviar" onclick="guardar()" type="button"  class="btn btn-success">
							<i class="fas fa-plus"></i>
							Guardar
								
							</button>
						
						</center>

						</form>

						<!--fin del formulario-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mdl-tabs__panel" id="tabListProvider">
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--8-col-desktop mdl-cell--2-offset-desktop">
				<div class="full-width panel mdl-shadow--2dp">
					<div class="full-width panel-tittle bg-success text-center tittles">
						Catalogo de cuentas
					</div>
					<div class="full-width panel-content">
						<form action="#">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
								<label class="mdl-button mdl-js-button mdl-button--icon" for="searchProvider">
									<i class="zmdi zmdi-search"></i>
								</label>
								<div class="mdl-textfield__expandable-holder">
									<input class="mdl-textfield__input" type="text" id="searchProvider">
									<label class="mdl-textfield__label"></label>
								</div>
							</div>
						</form>


						<!-- Aqui va la tabla -->
						<div class="responsible">
							<table class="table" border="2">
								<thead>
									<tr>
										<td>N°</td>
										<td>Cuenta</td>
									</tr>
								</thead>

								<tbody id="activo_cuenta" style="background-color:#e2c6f5 "></tbody>	

								<tbody id="activo_corriente" style="background-color: #8cf98f "></tbody>

								<tbody id="cuentas_activo_corriente"></tbody>

								<tbody id="activo_no_corriente" style="background-color:#8cf98f"></tbody>

								<tbody id="cuentas_activo_no_corriente"></tbody>

								<tbody id="otras_cuentas" style="background-color:#8cf98f"></tbody>

								<tbody id="cuentas_otras_cuentas"></tbody>

								<!--====================================================================================================-->

								<tbody id="pasivo_cuenta" style="background-color:#e2c6f5"></tbody>
								<tbody id="pasivo_a_corto_plazo" style="background-color: #8cf98f"></tbody>
								<tbody id="cuentas_pasivo_a_corto_plazo"></tbody>
								<tbody id="pasivo_a_largo_plazo" style="background-color: #8cf98f"></tbody>
								<tbody id="cuentas_pasivo_a_largo_plazo"></tbody>

								<!--====================================================================================================-->

								<tbody id="capital" style="background-color:#e2c6f5"></tbody>
								<tbody id="cuentas_capital"></tbody>

								<!--============================Ingresos=======================================-->

								<tbody id="ingresos" style="background-color:#e2c6f5"></tbody>
								<tbody id="ingresos_cuentas"></tbody>

								<!--=============================================================================-->

								<tbody id="costo" style="background-color:#e2c6f5"></tbody>

								<!--==============================================================================-->

								<tbody id="gastos" style="background-color:#e2c6f5"></tbody>
								<tbody id="gastos_cuentas"></tbody>

								<!--==============================================================================-->

								<tbody id="gastos"></tbody>
								<tbody id="gastos_cuentas"></tbody>

							</table>
						</div>

						<!--fin de tabla-->
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		cuenta_activo();
		activo_corriente();
		cuentas_activo_corriente();
		activo_no_corriente();
		cuentas_activo_no_corriente();
		otras_cuentas();
		cuentas_otras_cuentas();

		cuenta_pasivo();
		pasivo_a_corto_plazo();
		cuentas_pasivo_a_corto_plazo();
		pasivo_a_largo_plazo();
		cuentas_pasivo_a_largo_plazo();

		capital();
		cuentas_capital();

		ingresos();
		ingresos_cuentas();

		costo();

		gastos();
		gastos_cuentas();

	//================ PROCESO PARA GUARDAR ==========================//

		cuenta_perteneciente();
		típo_de_cuenta();

  jQuery(function(){
    jQuery( "#form" ).validate({
      rules: {
        id_cuenta:{
           required: true,                
        },
        nombre_cuenta:{
          required: true,
          alphas:true,
        },
        id_mayor:{
          required: true,
       },
       orden:{
          required: true,
       }
}
    });

 });


  jQuery.validator.addMethod("alphas", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
  }, 'Sólo caracteres');
  
  jQuery.validator.addMethod("numbers", function(value, element) {
  return this.optional(element) || /^[1-9]+$/.test(value);
  }, 'Sólo números');

  jQuery.validator.addMethod("alphas", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
  }, 'Sólo caracteres');
  
  jQuery.validator.addMethod("numbers", function(value, element) {
  return this.optional(element) || /^[1-9]+$/.test(value);
  }, 'Sólo números');

  $("#enviar").prop('disabled','disabled');
$("#form").on('keyup blur', function(){ //Evento cada que presionemos una tecla o posicion
  if ($("#form").valid()){
    //Habilitamos
    $("#enviar").prop('disabled', false);
  }else{
    //Deshabilitado
    $("#enviar").prop('disabled', 'disabled');
  }
});

	});
//==========================================Cuentas Activos ========================================================//




function cuenta_activo(){
	var url ='<?php echo base_url(); ?>catalogoCtr/cuenta_activo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#activo_cuenta').html(data);
		}
	});
}

function activo_corriente(){
	var url = '<?php echo base_url(); ?>catalogoCtr/activo_corriente';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#activo_corriente').html(data);
		}
	});
}

function cuentas_activo_corriente(){
	var url= '<?php echo base_url(); ?>catalogoCtr/cuentas_activo_corriente';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#cuentas_activo_corriente').html(data);
		}
	});
}

function activo_no_corriente(){
	var url= '<?php echo base_url(); ?>catalogoCtr/activo_no_corriente';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#activo_no_corriente').html(data);
		}
	});

}

function cuentas_activo_no_corriente(){
	var url= '<?php echo base_url(); ?>catalogoCtr/cuentas_activo_no_corriente';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#cuentas_activo_no_corriente').html(data);
		}
	});

}

function otras_cuentas(){
	var url= '<?php echo base_url(); ?>catalogoCtr/otras_cuentas';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#otras_cuentas').html(data);
		}
	});
}

function cuentas_otras_cuentas()
{
	var url= '<?php echo base_url(); ?>catalogoCtr/cuentas_otras_cuentas';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#cuentas_otras_cuentas').html(data);
		}
	}); 
}
//==================================================Cuentas de pasivo ===============================================================//

function cuenta_pasivo(){
	var url ='<?php echo base_url(); ?>catalogoCtr/cuenta_pasivo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#pasivo_cuenta').html(data);
		}
	});
}


function pasivo_a_corto_plazo(){
	var url = '<?php echo base_url(); ?>catalogoCtr/pasivo_a_corto_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#pasivo_a_corto_plazo').html(data);
		}
	});
}

function cuentas_pasivo_a_corto_plazo(){
	var url = '<?php echo base_url(); ?>catalogoCtr/cuentas_pasivo_a_corto_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#cuentas_pasivo_a_corto_plazo').html(data);
		}
	});
}

function pasivo_a_largo_plazo(){
	var url = '<?php echo base_url(); ?>catalogoCtr/pasivo_a_largo_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#pasivo_a_largo_plazo').html(data);
		}
	});
}

function cuentas_pasivo_a_largo_plazo(){
	var url = '<?php echo base_url(); ?>catalogoCtr/cuentas_pasivo_a_largo_plazo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#cuentas_pasivo_a_largo_plazo').html(data);
		}
	});
}

//=================================== Cuentas capital ============================================== //

function capital(){
	var url = '<?php echo base_url(); ?>catalogoCtr/capital';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#capital').html(data);
		}
	});
}

function cuentas_capital(){
	var url = '<?php echo base_url(); ?>catalogoCtr/cuentas_capital';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#cuentas_capital').html(data);
		}
	});
}

//=========================================== Ingresos ===========================================================//

function ingresos(){
	var url = '<?php echo base_url(); ?>catalogoCtr/ingresos';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#ingresos').html(data);
		}
	});
}

function ingresos_cuentas(){
	var url = '<?php echo base_url(); ?>catalogoCtr/ingresos_cuentas';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#ingresos_cuentas').html(data);
		}
	});
}
//================================================ Costos =======================================================//

function costo(){
	var url = '<?php echo base_url(); ?>catalogoCtr/costo';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#costo').html(data);
		}
	});
}

//================================================ Gastos ====================================================//

function gastos(){
	var url = '<?php echo base_url(); ?>catalogoCtr/gastos';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#gastos').html(data);
		}
	});
}

function gastos_cuentas(){
	var url = '<?php echo base_url(); ?>catalogoCtr/gastos_cuentas';
	$.ajax({
		type:'post',
		url: url,
		success:function(data){
			$('#gastos_cuentas').html(data);
		}
	});
}

//================================ PROCESO DE CGUARDAR PARTIDA ====================================

function cuenta_perteneciente(){
	var url = '<?php echo base_url('catalogoCtr/cuenta_perteneciente'); ?>';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#id_mayor').html(data);
		}
	});
}

function típo_de_cuenta(){
	var url = '<?php echo base_url(); ?>catalogoCtr/tipo_de_cuenta';
	$.ajax({
		type:'post',
		url:url,
		success:function(data){
			$('#orden').html(data);
		}
	});
}

function guardar(){
		var m='<?php echo base_url(); ?>catalogoCtr/agregar';
		$.ajax({
			url:m,
			type:'post',
			data: $('#form').serialize(),
			success: function(data){
				if(data==1){
					Swal({
						title: "Datos Ingresados Correctamente!",
						type: "success",
						showConfirmButton: false,
						timer: 1500

					});
                    
				}else{
					
					Swal({
						title: "Error al ingresar!",
						type: "error",
						showConfirmButton: false,
						timer: 1500

					});

				}
			}
		});
	}

</script>


