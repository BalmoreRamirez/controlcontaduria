<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class catalogoCtr extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('catalogoMold');
	}
//==============================================Cuentas Activos ============================================================//
	public function cuenta_activo()
	{
		$cuenta_activo = $this->catalogoMold->cuenta_activo();
		foreach($cuenta_activo as $ca){
			echo '<tr>';
			echo '<td>'.$ca['id_cuenta'].'</td>';
			echo '<td>'.$ca['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function activo_corriente()
	{
		$activo_corriente =$this->catalogoMold->activo_corriente();
		foreach($activo_corriente as $ac){
			echo '<tr>';
			echo '<td>'.$ac['id_cuenta'].'</td>';
			echo '<td>'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function cuentas_activo_corriente()
	{
		$cuentas_activo_corriente = $this->catalogoMold->cuentas_activo_corriente();
		foreach($cuentas_activo_corriente as $cac){
			echo '<tr>';
			echo '<td>'.$cac['id_cuenta'].'</td>';
			echo '<td>'.$cac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function activo_no_corriente()
	{
		$activo_no_corriente = $this->catalogoMold->activo_no_corriente();
		foreach($activo_no_corriente as $anc){
			echo '<tr>';
			echo '<td>'.$anc['id_cuenta'].'</td>';
			echo '<td>'.$anc['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function cuentas_activo_no_corriente()
	{
		$cuentas_activo_no_corriente = $this->catalogoMold->cuentas_activos_no_corrientes();
		foreach($cuentas_activo_no_corriente as $canc){
			echo '<tr>';
			echo '<td>'.$canc['id_cuenta'].'</td>';
			echo '<td>'.$canc['nombre_cuenta'].'</td>';
			echo '</tr>';		
		}
	}

	public function otras_cuentas()
	{
		$otras_cuentas = $this->catalogoMold->otras_cuentas();
		foreach($otras_cuentas as $ot){
			echo '<tr>';
			echo '<td>'.$ot['id_cuenta'].'</td>';
			echo '<td>'.$ot['nombre_cuenta'].'</td>';
			echo '</tr>';	
		}
	}

	public function cuentas_otras_cuentas()
	{
		$cuentas_otras_cuentas = $this->catalogoMold->cuentas_otras_cuentas();
		foreach($cuentas_otras_cuentas as $coc){
			echo '<tr>';
			echo '<td>'.$coc['id_cuenta'].'</td>';
			echo '<td>'.$coc['nombre_cuenta'].'</td>';
			echo '</tr>';		
		}
	}

//============================================Cuentas Pasivo==================================================================//

	public function cuenta_pasivo()
	{
		$cuenta_pasivo = $this->catalogoMold->cuenta_pasivo();
		foreach($cuenta_pasivo as $p){
			echo '<tr>';
			echo '<td>'.$p['id_cuenta'].'</td>';
			echo '<td>'.$p['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function pasivo_a_corto_plazo()
	{
		$pasivo_a_corto_plazo = $this->catalogoMold->pasivo_a_corto_plazo();
		foreach($pasivo_a_corto_plazo as $pacp){
			echo '<tr>';
			echo '<td>'.$pacp['id_cuenta'].'</td>';
			echo '<td>'.$pacp['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function cuentas_pasivo_a_corto_plazo()
	{
		$cuentas_pasivo_a_corto_plazo = $this->catalogoMold->cuentas_pasivo_a_corto_plazo();
		foreach($cuentas_pasivo_a_corto_plazo as $cpacp){
			echo '<tr>';
			echo '<td>'.$cpacp['id_cuenta'].'</td>';
			echo '<td>'.$cpacp['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function pasivo_a_largo_plazo()
	{
		$pasivo_a_largo_plazo = $this->catalogoMold->pasivo_a_largo_plazo();
		foreach($pasivo_a_largo_plazo as $palp){
			echo '<tr>';
			echo '<td>'.$palp['id_cuenta'].'</td>';
			echo '<td>'.$palp['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function cuentas_pasivo_a_largo_plazo()
	{
		$cuentas_pasivo_a_largo_plazo = $this->catalogoMold->cuentas_pasivo_a_largo_plazo();
		foreach($cuentas_pasivo_a_largo_plazo as $cpalp){
			echo '<tr>';
			echo '<td>'.$cpalp['id_cuenta'].'</td>';
			echo '<td>'.$cpalp['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

//============================================== Cuentas Capital ===================================================//

	public function capital()
	{
		$capital = $this->catalogoMold->capital();
		foreach($capital as $c){
			echo '<tr>';
			echo '<td>'.$c['id_cuenta'].'</td>';
			echo '<td>'.$c['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function cuentas_capital()
	{
		$cuentas_capital = $this->catalogoMold->cuentas_capital();
		foreach($cuentas_capital as $c){
			echo '<tr>';
			echo '<td>'.$c['id_cuenta'].'</td>';
			echo '<td>'.$c['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

//================================================ INGRESOS ===============================================================

	public function ingresos()
	{
		$ingresos = $this->catalogoMold->ingresos();
		foreach($ingresos as $i){
			echo '<tr>';
			echo '<td>'.$i['id_cuenta'].'</td>';
			echo '<td>'.$i['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function ingresos_cuentas()
	{
		$ingresos_cuentas = $this->catalogoMold->ingresos_cuentas();
		foreach($ingresos_cuentas as $ig){
			echo '<tr>';
			echo '<td>'.$ig['id_cuenta'].'</td>';
			echo '<td>'.$ig['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

//====================================================== Costo ============================================================//

public function costo()
	{
		$costo = $this->catalogoMold->costo();
		foreach($costo as $c){
			echo '<tr>';
			echo '<td>'.$c['id_cuenta'].'</td>';
			echo '<td>'.$c['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

//============================================== Gastos ===============================================================//

	public function gastos()
	{
		$gastos = $this->catalogoMold->gastos();
		foreach($gastos as $c){
			echo '<tr>';
			echo '<td>'.$c['id_cuenta'].'</td>';
			echo '<td>'.$c['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function gastos_cuentas()
	{
		$gastos_cuentas = $this->catalogoMold->gastos_cuentas();
		foreach($gastos_cuentas as $gc){
			echo '<tr>';
			echo '<td>'.$gc['id_cuenta'].'</td>';
			echo '<td>'.$gc['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

//=========================== PROCESO DE GUARDAR CATALOGO =====================================

	public function cuenta_perteneciente()
	{
		$mayor = $this->catalogoMold->cuenta_perteneciente();
		echo "<option value='0'>Cuenta perteneciente</option>";
		foreach($mayor as $m){
			echo "<option value='".$m['id_cuenta']."'>".$m['nombre_cuenta']."</option>";
		}
	}

	public function tipo_de_cuenta()
	{
	$orden = $this->catalogoMold->tipo_de_cuenta();
	echo "<option value='0'>Tipo de cuenta</option>";
	foreach($orden as $o){
		echo "<option value='".$o['id_orden']."'>".$o['nombre_orden']."</option>";
		} 
	}

	public function agregar()
	{
		$data['id_cuenta'] = $_POST['id_cuenta'];
		$data['nombre_cuenta'] = $_POST['nombre_cuenta'];
		$data['id_mayor'] = $_POST['id_mayor'];
		$data['orden'] = $_POST['orden'];
		echo $this->catalogoMold->insertar($data);
	}
}

