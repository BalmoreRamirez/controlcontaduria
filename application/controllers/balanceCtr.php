<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class balanceCtr extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('balance_model');
		$this->load->model('balancegeneral_model');
		$this->load->library('Fpdf1');

	}

	public function cuentas_corriente()
	{

		$fecha=$this->input->post('de');
		$fecha2=$this->input->post('acta');
		$corriente=$this->balance_model->getcuenta($fecha, $fecha2);
		
		$total=$this->balance_model->GetTotal($fecha,$fecha2);
		$empresa= $this->balancegeneral_model->empresa();	
		echo'<div class="full-width panel-tittle  text-center bg-info tittles" id="head">
		Balance de Comprobacion
		</div>
		<div class="full-width panel-tittle  text-center bg-info tittles" id="head">
		<table class="col-lg-12">
		<thead>
		<tr>
		<th>'.$empresa->nombre_e.'</th>
		<th>DE   '.$fecha.'</th>
		<th>HASTA '.$fecha2.'</th>
		</tr>
		</thead>
		</table>
		</div>

		<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp full-width">
		<thead>
		<tr>
		<th scope="col">#</th>
		<th scope="col">Cuenta</th>
		<th scope="col">Debito</th>
		<th scope="col">Credito</th>
		</tr>
		</thead>
		<tbody>';
		foreach ($corriente as  $c) {
			if ($c['type'] ==  2){
				echo "<tr>";
				echo "<td>".$c['id_cuenta']."</td>";
				echo "<td>".$c['nombre_cuenta']."</td>";

				echo "<td></td>";
				echo "<td>$".$c['monto']."</td>";
				echo "</tr>";
			}else{
				echo "<tr>";
				echo "<td>".$c['id_cuenta']."</td>";
				echo "<td>".$c['nombre_cuenta']."</td>";

				echo "<td>$".$c['monto']."</td>";
				echo "<td></td>";
				echo "</tr>";
			}
		}
		echo "<tr>";
		echo "<td></td>";
		echo "<td>TOTAL</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
		echo'</tbody>
		</table>
		';		
	}
	public function reporte(){
		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		
        $fecha=$this->input->get('de');
		$fecha2=$this->input->get('acta');
		$corriente=$this->balance_model->getcuenta($fecha, $fecha2);
		$pdf->SetFont('Arial','B',15);
		$pdf->Cell(30);
		$pdf->Cell(120,10,'Facela SA de CV',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'Balance Comprobacion',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10,'De '.$fecha.' Hasta '.$fecha2,0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'En Dolares Americanos',0,1,'C');
		$pdf->Ln(15);

		$pdf->SetFillColor(255,250,255);
		$pdf->SetFont('TIMES','',12);
		$pdf->Cell(20,6,'#',1,0,'C',1);
		$pdf->Cell(50,6,'nombre_cuenta',1,0,'C',1);
		$pdf->Cell(40,6,'fecha',1,0,'C',1);
		$pdf->Cell(40,6,'debito',1,0,'C',1);
		$pdf->Cell(40,6,'credito',1,1,'C',1);


		
		$balance=$this->balance_model->Get_();
		foreach ($balance as $ba) {
			if($ba['type']==2){
				$pdf->Cell(20,6,$ba['id_cuenta'],1,0,'',1);
				$pdf->Cell(50,6,$ba['nombre_cuenta'],1,0,'',1);
				$pdf->Cell(40,6,$ba['fecha_emitida'],1,0,'C',1);
				$pdf->Cell(40,6,'',1,0,'C',1);
				$pdf->Cell(40,6,'$'.$ba['monto'],1,1,'C',1);
			}else{
				$pdf->Cell(20,6,$ba['id_cuenta'],1,0,'',1);
				$pdf->Cell(50,6,$ba['nombre_cuenta'],1,0,'',1);
				$pdf->Cell(40,6,$ba['fecha_emitida'],1,0,'C',1);
				$pdf->Cell(40,6,'$'.$ba['monto'],1,0,'C',1);
				$pdf->Cell(40,6,'',1,1,'',1);
			}
		}
		$pdf->Cell(30,6,'',0,0,'',0);
		$pdf->Cell(40,6,'',0,0,'',0);
		$pdf->Cell(40,6,'TOTAL',1,0,'C',1);

		$balanc=$this->balance_model->Total();
		foreach ($balanc as $b) {
			$pdf->Cell(40,6,'$'.$b['Total'],1,0,'C',1);
		}

		$pdf->ln(40);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(90,6,'F.________________________',0,1,'L',1);
		$pdf->ln(1);
		$pdf->cell(60,6,'Contador',0,0,'C',1);
		$pdf->cell(60,6,'Representante legal',0,0,'C',1);
		$pdf->cell(60,6,'Auditor',0,0,'C',1);

		echo $pdf->Output('balancecomprobacion.pdf','I');
	}



}
