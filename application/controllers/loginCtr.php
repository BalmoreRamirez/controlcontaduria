<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginCtr extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		$usuarios= $this->login_model->getData();			
		$data['usuario']= $usuarios;	

		if($this->session->userdata("usuario")){
			$user = $this->session->userdata('usuario');
			$data['user'] = $this->login_model->get_usuario($user);
			
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu',$data);
			$this->load->view('home');
			$this->load->view('layouts/footer');
			
			}
			else{
			$this->load->view('layouts/header');
			$this->load->view('login',$data);
			$this->load->view('layouts/footer');	
		}	

    }
    	public function validar()
	{
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
		$resultado= $this->login_model->validar_login($usuario,$pass);

		if(!$resultado)
		{
			redirect(base_url());
		}
		else{

		$data = array (
						'id_usuario'=>$resultado->id_usuario,
						'usuario' => $resultado->usuario,
						'pass' => $resultado->pass,
						'login'=> true,

					   );

		$this->session->set_userdata($data);
		redirect('loginCtr/index');
    }
}

    public function logout()
    {
    	$this->session->sess_destroy();
    	redirect('loginCtr/index');
    }
}