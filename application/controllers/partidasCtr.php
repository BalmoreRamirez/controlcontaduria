<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class partidasCtr extends CI_Controller
{	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('partidas_model');
		$this->load->model('login_model');
	}
	public function select_cuenta()
	{
		$tipopar = $this->partidas_model->tipo(); 
		echo "<option value='0'></option>";
		foreach ($tipopar as $key ) {
			echo "<option value='".$key['id_tipo_partida']."'>".$key['nombre_tipo_partida']."</option>";			
		}
	}
	public function select()
	{
		$cuenta = $this->partidas_model->cuenta();
		echo "<option value='0'></option>";
		foreach($cuenta as $c){
			echo "<option value='".$c['id_cuenta']."'>".$c['nombre_cuenta']."</option>";
		}
	}
	public function select_tipo()
	{
		$id_mayor = $this->input->get('id');
		$cuenta = $this->partidas_model->sel_mayo($id_mayor);
		echo "<option value='0'></option>";
		foreach($cuenta as $c){
			echo "<option value='".$c['id_cuenta']."'>".$c['nombre_cuenta']."</option>";
		}
	}
	public function select_sub()
	{
		$id_mayor = $this->input->get('id');
		$cuenta = $this->partidas_model->sel_sub($id_mayor);
		echo "<option value='0'></option>";
		foreach($cuenta as $c){
			echo "<option value='".$c['id_cuenta']."'>".$c['nombre_cuenta']."</option>";
		}
	}
	public function cuenta_partida()
	{
		$id_cuenta = $this->input->post('id_cuenta');
		$monto = $this->input->post('monto');
		$operacion = $this->input->post('operacion');
		$data= $this->partidas_model->guardar_cuentas_partidas($id_cuenta,$monto,$operacion);
		echo json_encode($data);
	}
	public function prue(){

		$max = $this->partidas_model->max_id();

		
		echo $max->max;
	}
	public function agregar_partidas()
	{
		$user = $this->session->userdata('usuario');	
		$id = $this->login_model->get_usuario($user);
		$id_user = $id->id_usuario;
		$maxs = $this->partidas_model->max_partida($id_user);

		$data['id'] = $maxs->max;
		$cuenta = $_POST['id_cuenta'];
		$data['monto'] = $_POST['monto'];
		$data['fecha'] = $_POST['fecha'];
		$data['fechas'] = date("Y-m-d");
		$data['id_tipo'] = $_POST['tipo_partida'];
		$data['id_usuario'] = $id_user;

		$data['estado'] = 1;
		$primaria = $_POST['id_orden'];
		$secundaria = $_POST['id_mayor'];
		
		
		if ($secundaria  == 0) {
			$this->partidas_model->partida_guardar($data,$primaria);
		}else{
			$this->partidas_model->partida_guardas($data,$secundaria);
		}
		
	}
	public function partidas()
	{
		if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {

			$user = $this->session->userdata('usuario')	;	
			$id = $this->login_model->get_usuario($user);
			$id_user = $id->id_usuario;
			$maxs = $this->partidas_model->max_partida($id_user);
			$id_max = $maxs->max;
			$partidas = $this->partidas_model->mostrarse($id_user,$id_max);

			foreach($partidas as $ca){
				echo "<tr>";
				echo "<td>".$ca['id_partida'].
				"</td>";
				echo '<td>'.$ca['nombre_cuenta'].'</td>';
				echo '<td>'."$".$ca['monto'].'</td>';
				echo '<td>'.$ca['nombre_tipo_partida'].'</td>';

				echo "</tr>";
			}
		}
	}
	public function terminar()
	{
		$user = $this->session->userdata('usuario')	;	
		$id = $this->login_model->get_usuario($user);
		$id_user = $id->id_usuario;
		$maxs = $this->partidas_model->max_partida($id_user);
		$id_max = $maxs->max;
		$partidas = $this->partidas_model->extraer_par_cuenta($id_user,$id_max);

		foreach($partidas as $ca){
			$id = $ca['id_cuenta_partida'];
			$data = 2;
			$this->partidas_model->update_cuenta_par($data,$id);
		}
	}
	public function buscar(){
		if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {

			$user = $this->session->userdata('usuario')	;	
			$id = $this->login_model->get_usuario($user);
			$id_usuario = $id->id_usuario;
			echo $this->partidas_model->buscar($id_usuario);
		}else{
			redirect('loginCtr/index');
		}	
	}
	public function vista(){
		if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {

			$user = $this->session->userdata('usuario')	;	
			$data['user'] = $this->login_model->get_usuario($user);
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu',$data);
			$this->load->view('modal/modales');
			$this->load->view('ingresar_partida');
			$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
	}
	public function generar_partida(){

		$user = $this->session->userdata('usuario')	;	
		$id_user = $this->login_model->get_usuario($user);
		$codigo = $this->partidas_model->codigo();
		$data['id_usuario'] = $id_user->id_usuario;
		$data['codigo'] = $codigo->codigo+1;
		$this->partidas_model->insert_partida($data);

		redirect('Inicial/tab_cuentas');
	}
//
}

