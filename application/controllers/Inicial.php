<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inicial extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('login_model');
			$this->load->model('partidas_model');
			$this->load->model('balancegeneral_model');
			$this->load->model('balance_model');
			$this->load->model('estadoResulMold');

		}
	public function index()
	{			
		if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {

			$user = $this->session->userdata('usuario');
			$data['user'] = $this->login_model->get_usuario($user);
				$this->load->view('layouts/header');
				$this->load->view('layouts/menu',$data);
				$this->load->view('modal/modales');
				$this->load->view('Home');
				$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		
	}
	//============================================================
	public function CantaCuentas()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
		$user = $this->session->userdata('usuario');				
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');		
		$this->load->view('CantaCuentas');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

		
	}
	//============================================================
	public function BalanceCompro()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
		$user = $this->session->userdata('usuario');
		$data['empresa'] = $this->balancegeneral_model->empresa();			
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('BalanceCompro');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

		
	}

	//============================================================
	public function Costos()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
		$user = $this->session->userdata('usuario');				
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('Costos');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

		
	}
	//============================================================
	public function EstaPerdidGanan()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
		$user = $this->session->userdata('usuario');				
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('EstaPerdidGanan');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

		
	}
		//============================================================
	public function Gastos()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario');		
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('Gastos');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

	}
	//============================================================
	public function Partidas()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario')	;	
		$data['user'] = $this->login_model->get_usuario($user);
		$data['codigo'] = $this->partidas_model->codigo();

		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('Partidas',$data);
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

	} 
	/*public function Partidas()
	{if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario')	;	
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('Partidas');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		

	} 
*/

public function EstadoResultaso()
{
	if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario')	;
		$de = $this->input->post("de");
		$acta = $this->input->post("acta");

		$data['fecha']= $this->estadoResulMold->cargarInfo($de,$acta);
		$data['empresa'] = $this->balancegeneral_model->empresa();			
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('EstadoResultado');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		
}

public function tab_cuentas()
{
	if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario')	;	
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('tab_cuentas');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		
}

public function balance_general()
{
	if ($this->session->userdata('usuario') !=NULL|| $this->session->userdata('usuario')!='') {
						
		$user = $this->session->userdata('usuario')	;
		$data['empresa'] = $this->balancegeneral_model->empresa();	
		$data['user'] = $this->login_model->get_usuario($user);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu',$data);
		$this->load->view('modal/modales');
		$this->load->view('balance_general');
		$this->load->view('layouts/footer');
		}else{
			redirect('loginCtr/index');
		}	
		
}





}
?>