<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class balancegeneral extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('balancegeneral_model');
		$this->load->model('catalogoMold');
		$this->load->library('Fpdf1');
		
		

	}

	public function activos()
	{
		$partidas = $this->balancegeneral_model->activo();

		foreach($partidas as $p){
			echo "<tr>";
			echo "<td>".$p['nombre_cuenta']."</td>";
			echo "<td >".$p['monto']."</td>";
			echo "</tr>";
		}
	}

	public function total_activos()
	{
		$total = $this->balancegeneral_model->total_a();
		echo "<tr>";

		echo "<td>toal activos corrientes</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

	public function activo_corriente()
	{
		$activo_corriente =$this->balancegeneral_model->activo_corriente();
		foreach($activo_corriente as $ac){
			echo '<tr>';
			echo '<td colspan="2">'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function activo_no_corriente()
	{
		$activo_no_corriente = $this->balancegeneral_model->activo_no_corriente();
		foreach($activo_no_corriente as $anc){
			echo '<tr>';
			echo '<td colspan="2">Activo fijo</td>';
			echo '</tr>';
		}
	}

	public function activo_fijo()
	{
		$activo_fijo = $this->balancegeneral_model->activo_fijo();
		foreach($activo_fijo as $anc){
			echo '<tr>';
			echo '<td>'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function total_activo_fijo()
	{
		$total = $this->balancegeneral_model->total_activo_fijo();
		echo "<tr>";
		echo "<td>total activo fijo</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

	public function total_activo()
	{
		$total = $this->balancegeneral_model->total_activo();
		echo "<tr>";
		echo "<td>total activo </td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

//======================================= Pasivos ==================================================

	public function pasivo_corriente()
	{
		$pasivo_corriente =$this->balancegeneral_model->pasivo_corriente();
		foreach($pasivo_corriente as $ac){
			echo '<tr>';
			echo '<td colspan="2">'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function pasivos()
	{
		$partida = $this->balancegeneral_model->pasivo();

		foreach($partida as $p){
			echo "<tr>";
			echo "<td>".$p['nombre_cuenta']."</td>";
			echo "<td >".$p['monto']."</td>";
			echo "</tr>";
		}
	}

	public function total_pasivos()
	{
		$total = $this->balancegeneral_model->total_p();
		echo "<tr>";
		echo "<td>Total pasivo a corto plazo</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

	public function pasivo_largo_plazo()
	{
		$pasivo_corriente =$this->balancegeneral_model->pasivo_largo_plazo();
		foreach($pasivo_corriente as $ac){
			echo '<tr>';
			echo '<td colspan="2">'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function mas_pasivo_largo_plazo()
	{
		$mas_pasivo_corriente =$this->balancegeneral_model->mas_pasivo_largo_plazo();
		foreach($mas_pasivo_corriente as $ac){
			echo '<tr>';
			echo '<td colspan="2">'.$ac['nombre_cuenta'].'</td>';
			echo '</tr>';
		}
	}

	public function total_pasivos_largo_plazo()
	{
		$total = $this->balancegeneral_model->total_p_largo_plazo();
		echo "<tr>";
		echo "<td>Total pasivo a corto plazo</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

//================================ PATRIMONIO ===========================================

	public function patrimonio()
	{	
		echo "<tr>";
		echo "<td>PATRIMONIO</td>";
		echo "</tr>";
	}

	public function patrimonio_cuentas()
	{
		$patrimonio_cuentas =$this->balancegeneral_model->patrimonio_cuentas();
		foreach($patrimonio_cuentas as $ac){
			echo '<tr>';
			echo '<td>'.$ac['nombre_cuenta'].'</td>';
			echo '<td>'.$ac['monto'].'</td>';
			echo '</tr>';
		}
	}

	public function total_patrimonio()
	{
		$total = $this->balancegeneral_model->total_patrimonio();
		echo "<tr>";
		echo "<td>Total patrimonio</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}

	public function total_patrimonio_mas_capital()
	{
		$total = $this->balancegeneral_model->total_patrimonio_mas_pasivo();
		echo "<tr>";
		echo "<td>Total patrimonio + capital</td>";
		foreach ($total as $t) {
			echo "<td>$".$t['Total']."</td>";			
		}
		echo "</tr>";
	}
	



    public function reporte()
    {
	    $pdf = new FPDF();
	    $pdf->AliasNbPages();
	    $pdf->AddPage();

	    $pdf->SetFont('Arial','B',15);
		$pdf->Cell(30);
		$pdf->Cell(120,10,'Facela SA de CV',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'Balance General',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10,"AL ".date("d-m-Y"),0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'En Dolares Americanos',0,1,'C');
		$pdf->Ln(20);

		$pdf->Cell(40);
		$pdf->SetFillColor(250,250,250);
		$pdf->SetFont('TIMES','',12);
		$pdf->Cell(48,6,'Activo',1,0,'C',1);
		$pdf->Cell(48,6,'Monto',1,1,'C',1);
		
		$pdf->Cell(40);
		$balance=$this->catalogoMold->activo_corriente();
		$balance1=$this->balancegeneral_model->activo();
		$total=$this->balancegeneral_model->total_activo_fijo();
		$tota1=$this->balancegeneral_model->total_activo();
		
             
		foreach ($balance as $ba) {
			$pdf->Cell(96,6,$ba['nombre_cuenta'],1,1,'L',1);
		}
		$pdf->Cell(40);
		foreach ($balance1 as $ba) {
			$pdf->Cell(48,6,$ba['nombre_cuenta'],1,0,'C',1);
			$pdf->Cell(48,6,'$'.$ba['monto'],1,1,'C',1);
			$pdf->Cell(40);
		}

		foreach ($total as $to) {
			$pdf->Cell(48,6,'Total Activo fijo',1,0,'C',1);

			$pdf->Cell(48,6,'-'.$to['Total'],1,1,'C',1);
			$pdf->Cell(40);
		}  
		foreach ($tota1 as $to) {
			$pdf->Cell(48,6,'Total Activo',1,0,'C',1);
			$pdf->Cell(48,6,'$'.$to['Total'],1,1,'C',1);
		}  
		$pdf->ln();
		$pdf->Cell(40);
		$pdf->Cell(48,6,'Capital',1,0,'C',1);
		$pdf->Cell(48,6,'Monto',1,1,'C',1);

		$pdf->Cell(40);
		$pasivo=$this->balancegeneral_model->pasivo();
		$pasivo1=$this->balancegeneral_model->pasivo_corriente();
		$total=$this->balancegeneral_model->total_p();
		$tota1=$this->balancegeneral_model->total_p_largo_plazo();
		$totalp=$this->balancegeneral_model->total_patrimonio();
		$to=$this->balancegeneral_model->total_patrimonio_mas_pasivo();
		$patrimonio=$this->balancegeneral_model->patrimonio_cuentas();


		foreach ($pasivo1 as $p) {
			$pdf->cell(96,6,$p['nombre_cuenta'],1,1,'',1);
		}
		$pdf->Cell(40);
		foreach ($pasivo as $p) {
			$pdf->cell(50,6,$p['nombre_cuenta'],1,0,'',1);
			$pdf->Cell(46,6,'$'.$p['monto'],1,1,'C',1);
			$pdf->Cell(40);
		}
		foreach ($total as $t) {
			$pdf->cell(50,6,'Total Pasivo a Corto plazo',1,0,'',1);
			$pdf->Cell(46,6,'$'.$t['Total'],1,1,'C',1);
			
		}

		$pdf->Cell(40);
		$pdf->cell(96,6,'Pasivos a Largo Plazo',1,1,'',1);
		$pdf->Cell(40); 
		foreach ($tota1 as $t) {

			$pdf->cell(50,6,'Total Pasivo a Largo Plazo',1,0,'',1);
			$pdf->Cell(46,6,'-'.$t['Total'],1,1,'C',1);
			$pdf->ln(6);
			$pdf->Cell(40);
			
		}
		foreach ($patrimonio as $p) {

			
			$pdf->Cell(50,6,$p['nombre_cuenta'],1,0,'L',1);
			$pdf->Cell(46,6,'$'.$p['monto'],1,1,'C',1);
			$pdf->Cell(40);
		}
       
		foreach ($totalp as $t) {

			$pdf->cell(50,6,'Total patrimonio',1,0,'',1);
			$pdf->Cell(46,6,'$'.$t['Total'],1,1,'C',1);
			$pdf->Cell(40);
		}
        foreach ($to as $t) {
			$pdf->cell(50,6,'Total patrimonio + Capital',1,0,'',1);
			$pdf->Cell(46,6,'$'.$t['Total'],1,1,'C',1);
			$pdf->Cell(40);
		}
		$pdf->ln(40);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(90,6,'F.________________________',0,1,'L',1);
		$pdf->ln(1);
		$pdf->cell(60,6,'Contador',0,0,'C',1);
		$pdf->cell(60,6,'Representante legal',0,0,'L',1);
		$pdf->cell(60,6,'Auditor',0,0,'C',1);


		echo $pdf->Output('balancegeneral.php','I');

	}
}