<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class estadoResultadoContr extends CI_Controller
{

	protected $col=0;//columna actual
	protected $y0;//ordenada de comienzo de la columna

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('estadoResulMold');
		$this->load->model('balancegeneral_model');
		$this->load->library('fpdf1');
	}
	public function consultar()
	{
		$de = $this->input->post("de");
		$acta = $this->input->post("acta");

		$data= $this->estadoResulMold->cargarInfo($de,$acta);
		$empresa = $this->balancegeneral_model->empresa();	

		// $this->load->view('EstadoResultado'$data);
		$ingresos = $this->estadoResulMold->ingresos($de,$acta);


		$costos = $this->estadoResulMold->costos($de,$acta);

		$gastos = $this->estadoResulMold->gastos($de,$acta);

		//$intereses = $this->estadoResulMold->intereses($de,$acta);


		$utilidadBruta = ($ingresos->m) - ($costos->co);

		$utlidadOperativa = ($utilidadBruta) - ($gastos->gas);

		$intereses = ($utlidadOperativa * 0.3);

		$utilidadNeta = (($utlidadOperativa)-($intereses));

		//==utilidad operativa
	//	$utlidadoperativa  = $utilidadBruta - $gastos;
//https://www.youtube.com/watch?v=BJ97-EZh7xQ
		//===========utilidad neta

		//$utlidadNeta =  $utlidadoperativa - $intereses;
			//$ingresos = $this->estadoResulMold->ingresos();
		//echo json_encode($data);
		echo "
		<div class='card'>
		<div class='card-header text-center text-white bg-info'>ESTADO DE RESULTADO</div>
		<div class='card-body'>
		<div>
		<table class='table'>
		<thead class='thead-info'>
		<tr class='text-center text-white bg-info'>
		<th scope='col'>".$empresa->nombre_e."</th>
		<th scope='col'> DE ".$de."</th>
		<th scope='col'> Hasta ".$acta."</th>
		</tr>
		<tr>
		<th scope='col'>Cuenta</th>
		<th scope='col'  colspan='2' class='text-center'>Cantidad</th>
		</tr>
		</thead>
		<tbody id='estados'>";
		echo "<tr>";
		echo "<td>Ingresos</td>";
		echo "<td  colspan='2'>$".$ingresos->m."</td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td>Costo de Operacion</td>";
		echo "<td  colspan='2'>$".$costos->co."</td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td><strong>Utilidad Bruta</strong></td>";
		echo "<td  colspan='2'><strong> $".$utilidadBruta."</strong></td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td>Gastos Operativos</td>";
		echo "<td  colspan='2'> $".$gastos->gas."</td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td><strong>Utilidad Operativa</strong></td>";
		echo "<td  colspan='2'><strong>$".$utlidadOperativa."</strong></td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td>Impuesto  de (0.3)</td>";
		echo "<td  colspan='2'>$".$intereses."</td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td><strong>Utilidad neta</strong></td>";
		echo "<td  colspan='2'><strong>$".$utilidadNeta."</strong></td>";
		echo "</tr>";

		echo"</tbody>
		</table>
		</div>
		</div>
		</div>";

		
	}
	public function utilidadaNeta()
	{

		$ingresos = $this->estadoResulMold->ingresos();
		//consultas
	}
	public function Reporte_estado(){
		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$de = $this->input->get("de");
		$acta = $this->input->get("acta");

		$pdf->SetFont('Arial','B',15);
		$pdf->Cell(30);
		$pdf->Cell(120,10,'Facela SA de CV',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'Estado De Resultado',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'En Dolares Americanos',0,1,'C');
		$pdf->Cell(30);
		$pdf->Cell(120,10, 'DE '.$de.' Hasta '.$acta,0,1,'C');
		$pdf->Ln(15);


		
		$pdf->SetFont('TIMES','',12);
		$pdf->cell(40);
		$pdf->SetFillColor(235,222,201);
		$pdf->Cell(50,6,'nombre de cuenta',0,0,'C',1);
		$pdf->cell(50,6,'Monto',0,1,'C',1);
		$pdf->SetFillColor(255,250,255);
		$pdf->Cell(40);

		/*$data= $this->estadoResulMold->cargarInfop();*/
		$ingresos = $this->estadoResulMold->ingresosp();
		$costos = $this->estadoResulMold->costosp();
		$gastos = $this->estadoResulMold->gastosp();
		$interesesp = $this->estadoResulMold->interesesp();
		
		$utilidadBruta = ($ingresos->m) - ($costos->co);

		$utlidadOperativa = (($utilidadBruta) - ($gastos->gas));

		$intereses = ($utlidadOperativa * 0.3);

		$utilidadNeta = ($utlidadOperativa)-(($intereses));

		$utilidadNeta = $utlidadOperativa-$intereses;


		$utlidadOperativa = ($utilidadBruta) - ($gastos->gas);



		$pdf->Cell(50,6,'Ingreso',1,0,'C',1);
		$pdf->Cell(50,6,'$'.$ingresos->m,1,1,'C',1);
		$pdf->cell(40);
		$pdf->Cell(50,6,'Costo de operacion',1,0,'C',1);
		$pdf->Cell(50,6,'$'.$costos->co,1,1,'C',1);
		$pdf->Cell(40);
		$pdf->SetFont('TIMES','B',12);
		$pdf->Cell(50,6,'Utilidad Bruta',1,0,'C',1);
		$pdf->SetFont('TIMES','',12);
		$pdf->Cell(50,6,'$'.$utilidadBruta,1,1,'C',1);
		$pdf->Cell(40);
		$pdf->Cell(50,6,'Gasto Operativos',1,0,'C',1);
		$pdf->Cell(50,6,'$'.$gastos->gas,1,1,'C',1);
		$pdf->Cell(40);
		$pdf->SetFont('TIMES','B',12);
		$pdf->Cell(50,6,'Utilidad Operativa',1,0,'C',1);
		$pdf->SetFont('TIMES','',12);
		$pdf->Cell(50,6,'$'.$utlidadOperativa,1,1,'C',1);
		$pdf->Cell(40);
		$pdf->Cell(50,6,'Impuestos de (0.3)',1,0,'C',1);
		$pdf->Cell(50,6,'$'.$intereses,1,1,'C',1);		
		$pdf->Cell(40);
		$pdf->SetFont('TIMES','B',12);
		$pdf->Cell(50,6,'Utilidad Neta',1,0,'C',1);
		$pdf->SetFont('TIMES','',12);
		$pdf->Cell(50,6,'$'.$utilidadNeta,1,1,'C',1);
		$pdf->Cell(40);


		$pdf->ln(40);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(60,6,'F.________________________',0,0,'L',1);
		$pdf->cell(90,6,'F.________________________',0,1,'L',1);
		$pdf->ln(1);
		$pdf->cell(60,6,'Contador',0,0,'C',1);
		$pdf->cell(60,6,'Representante legal',0,0,'C',1);
		$pdf->cell(60,6,'Auditor',0,0,'C',1);

		echo $pdf->Output('estado_resultado.php','I');
	}
}