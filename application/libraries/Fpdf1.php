 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fpdf1{
		
	public function __construct() {
		
		require_once APPPATH.'third_party/fpdf/fpdf.php';
		
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->Header();

	
		
		$CI =& get_instance();
		$CI->fpdf = $pdf;
		
	}
	 public function Header()
	{   

		$pdf->SetFont('Arial','B',15);
		$pdf->Cell(50);
		$pdf->Cell(120,10, 'Reporte',0,0,'C');
		$pdf->Ln(20);
	}

	public function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','I', 8);
		$this->Cell(0,10, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C' );
	}	
    public function getInstance(){
		return new Fpdf1();
	}

}

